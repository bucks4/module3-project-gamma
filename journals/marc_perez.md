# 26Jun23

First day starting the project. We ensured our yaml file would boot
properly and all agreed to rewatch the back end auth and FastAPI
videos one more time to full send starting tomorrow. Team currently has
no blockers.

27Jun23

Second day with the project. Back end auth implemented. Team currently has no blockers.

11Jul23
Worked through a sorting issue for the leaderboard. We were trying to mutate an immutable object. added slice to the map line.

17Jul23
Attempted to do a carousel on the main page. Buttons not working and is not auto cycling

18Jul23
Carousel issue fixed. Buttons and auto cycling complete. Added featured bird test. Test runs with no issues.
