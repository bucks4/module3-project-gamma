import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const birdApi = createApi({
  reducerPath: "birdApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getAllBirds: builder.query({
      query: () => "/api/birds",
    }),
    getAllUsers: builder.query({
      query: () => "/api/accounts",
    }),
    getAllBirdSightings: builder.query({
      query: () => "/api/bird_sightings",
      providesTags: ["sightings"],
    }),
    getsingleBirdSighting: builder.query({
      query: (bird_id) => `/api/single_bird_sighting/${bird_id}`,
      providesTags: ["sightings"],
    }),
    getAllUserBirdSightings: builder.query({
      query: () => ({
        url: `/api/user_bird_sightings`,
        credentials: "include",
      }),
      providesTags: ["user_sightings"],
    }),
    deleteUserBirdSightings: builder.mutation({
      query: (bird_sighting_id) => ({
        url: `/api/bird_sightings/${bird_sighting_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["user_sightings", "sightings"],
    }),
    getAllFeaturedBirds: builder.query({
      query: () => "/api/featured_bird",
    }),
    getAccount: builder.query({
      query: () => ({
        url: `/token`,
        credentials: "include",
      }),
      transformResponse: (response) => response?.account || null,
      providesTags: ["Account"],
    }),
    editBirdSighting: builder.mutation({
      query: ({ bird_id, ...body }) => ({
        url: `/api/bird_sightings/${bird_id}`,
        method: "PUT",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["user_sightings", "sightings"],
    }),
    postSighting: builder.mutation({
      query: (body) => {
        return {
          url: "/api/bird_sightings",
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["user_sightings", "sightings"],
    }),
    signup: builder.mutation({
      query: (body) => {
        return {
          url: "/api/accounts",
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: `/token`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),
    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: `/token`,
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account"],
    }),
  }),
});

export const {
  useGetAllBirdsQuery,
  useGetAllUsersQuery,
  useGetAllUserBirdSightingsQuery,
  useGetAccountQuery,
  usePostSightingMutation,
  useDeleteUserBirdSightingsMutation,
  useSignupMutation,
  useLogoutMutation,
  useLoginMutation,
  useGetAllBirdSightingsQuery,
  useGetAllFeaturedBirdsQuery,
  useEditBirdSightingMutation,
  useGetsingleBirdSightingQuery,
} = birdApi;
