steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE birds (
            id SERIAL PRIMARY KEY NOT NULL,
            bird_name VARCHAR(100) NOT NULL UNIQUE,
            picture_url VARCHAR(500) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE birds;
        """,
    ],
]
