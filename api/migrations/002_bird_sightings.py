steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE bird_sightings (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id SMALLINT REFERENCES accounts(id) NOT NULL,
            name VARCHAR(100) NOT NULL,
            date DATE NOT NULL,
            notes VARCHAR(5000),
            picture_url VARCHAR(500)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE bird_sightings;
        """,
    ],
]
