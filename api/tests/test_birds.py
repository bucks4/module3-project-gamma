from fastapi.testclient import TestClient
from main import app
from queries.birds import birdsQueries

client = TestClient(app)


class FakeBirdQueries:
    def get_all_birds(self):
        return {
            "Birds": [
                {
                    "id": 1,
                    "bird_name": "Pied-billed Grebe",
                    "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304522011/900",  # noqa
                },
            ]
        }


def test_list_all_birds():
    app.dependency_overrides[birdsQueries] = FakeBirdQueries

    res = client.get("/api/birds")
    data = res.json()

    assert data == {
        "Birds": [
            {
                "id": 1,
                "bird_name": "Pied-billed Grebe",
                "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304522011/900",  # noqa
            },
        ]
    }
