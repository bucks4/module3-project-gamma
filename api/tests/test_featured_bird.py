from fastapi.testclient import TestClient
from main import app
from queries.featured_bird import featured_birdQueries

client = TestClient(app)


class FakeBirdQueries:
    def get_all_featured_birds(self):
        return {
            "Birds": [
                {
                    "id": 2,
                    "name": "Bald Eagle",
                    "description": "Scavenges and hunts near bodies of water. Soars with wings flat, like a large, dark plank. Head appears large in flight; projects far in front of wings. Surprisingly weak-sounding vocalization is a series of high-pitched whistles.",  # noqa
                    "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306062281/900",  # noqa
                },
            ]
        }


def test_featured_birds():
    app.dependency_overrides[featured_birdQueries] = FakeBirdQueries  # noqa
    res = client.get("/api/featured_bird")
    data = res.json()
    assert data == {
        "Birds": [
            {
                "id": 2,
                "name": "Bald Eagle",
                "description": "Scavenges and hunts near bodies of water. Soars with wings flat, like a large, dark plank. Head appears large in flight; projects far in front of wings. Surprisingly weak-sounding vocalization is a series of high-pitched whistles.",  # noqa
                "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306062281/900",  # noqa
            },
        ]
    }


# tests by Marc P
