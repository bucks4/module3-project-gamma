import os
from psycopg_pool import ConnectionPool


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])

birds = [
    {
        "bird_name": "Pied-billed Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304522011/900",  # noqa
    },
    {
        "bird_name": "Horned Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304534481/900",  # noqa
    },
    {
        "bird_name": "Red-necked Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/65057131/900", # noqa
    },
    {
        "bird_name": "Eared Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305508041/900",  # noqa
    },
    {
        "bird_name": "Western Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305516121/900",  # noqa
    },
    {
        "bird_name": "Clark's Grebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/164017921/900",  # noqa
    },
    {
        "bird_name": "American White Pelican",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304461551/900",  # noqa
    },
    {
        "bird_name": "American Bittern",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304465371/900",  # noqa
    },
    {
        "bird_name": "Great Blue Heron",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304479371/900",  # noqa
    },
    {
        "bird_name": "White-faced Ibis",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308512681/900",  # noqa
    },
    {
        "bird_name": "Tundra Swan",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/59956021/900",  # noqa
    },
    {
        "bird_name": "Trumpeter Swan",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295430011/900",  # noqa
    },
    {
        "bird_name": "Greater White-fronted Goose",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/59938311/900",  # noqa
    },
    {
        "bird_name": "Snow Goose",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295417571/900",  # noqa
    },
    {
        "bird_name": "Ross's Goose",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295421201/900",  # noqa
    },
    {
        "bird_name": "Canada Goose",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/59953191/900",  # noqa
    },
    {
        "bird_name": "Wood Duck",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/65533521/900",  # noqa
    },
    {
        "bird_name": "Green-winged Teal",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63893401/900",  # noqa
    },
    {
        "bird_name": "Mallard",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308743051/900",  # noqa
    },
    {
        "bird_name": "Northern Pintail",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301309501/900",  # noqa
    },
    {
        "bird_name": "Blue-winged Teal",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/244380641/900",  # noqa
    },
    {
        "bird_name": "Cinnamon Teal",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299913341/900",  # noqa
    },
    {
        "bird_name": "Northern Shoveler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/300124811/900",  # noqa
    },
    {
        "bird_name": "Gadwall",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308742131/900",  # noqa
    },
    {
        "bird_name": "Eurasian Wigeon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/300137371/900",  # noqa
    },
    {
        "bird_name": "American Wigeon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60017891/900",  # noqa
    },
    {
        "bird_name": "Canvasback",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63893981/900",  # noqa
    },
    {
        "bird_name": "Redhead",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301323281/900",  # noqa
    },
    {
        "bird_name": "Ring-necked Duck",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301536921/900",  # noqa
    },
    {
        "bird_name": "Greater Scaup",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/162823881/900",  # noqa
    },
    {
        "bird_name": "Lesser Scaup",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63896301/900",  # noqa
    },
    {
        "bird_name": "Stilt Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301261561/900",  # noqa
    },
    {
        "bird_name": "Short-billed Dowitcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301552911/900",  # noqa
    },
    {
        "bird_name": "Long-billed Dowitcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301557841/900",  # noqa
    },
    {
        "bird_name": "Common Snipe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/45198111/900",  # noqa
    },
    {
        "bird_name": "Wilson's Phalarope",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64831941/900",  # noqa
    },
    {
        "bird_name": "Red-necked Phalarope",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301571851/900",  # noqa
    },
    {
        "bird_name": "Red Phalarope",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/164867911/900",  # noqa
    },
    {
        "bird_name": "Franklin's Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/169529071/900",  # noqa
    },
    {
        "bird_name": "Bonaparte's Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303339391/900",  # noqa
    },
    {
        "bird_name": "Black Tern",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/320036271/900",  # noqa
    },
    {
        "bird_name": "Ring-billed Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303369661/900",  # noqa
    },
    {
        "bird_name": "California Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303379191/900",  # noqa
    },
    {
        "bird_name": "Herring Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303381091/900",  # noqa
    },
    {
        "bird_name": "Band-Tailed Pigeon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/164025671/900",  # noqa
    },
    {
        "bird_name": "Glaucous Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303396771/900",  # noqa
    },
    {
        "bird_name": "Sabine's Gull",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303337391/900",  # noqa
    },
    {
        "bird_name": "Caspian Tern",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308395131/900",  # noqa
    },
    {
        "bird_name": "Common Tern",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299890491/900",  # noqa
    },
    {
        "bird_name": "Forster's Tern",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308420601/900",  # noqa
    },
    {
        "bird_name": "Rock Dove",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308065631/2400",  # noqa
    },
    {
        "bird_name": "Band-tailed Pigeon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/164025671/1200",  # noqa
    },
    {
        "bird_name": "Mourning Dove",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/203565681/1200",  # noqa
    },
    {
        "bird_name": "Black-billed Cuckoo",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308464851/1200",  # noqa
    },
    {
        "bird_name": "Barn Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297342281/1200",  # noqa
    },
    {
        "bird_name": "Flammulated Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297354921/1200",  # noqa
    },
    {
        "bird_name": "Western Screech-Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311369771/1200",  # noqa
    },
    {
        "bird_name": "Great Horned Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297363481/1200",  # noqa
    },
    {
        "bird_name": "Snowy Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297365891/1200",  # noqa
    },
    {
        "bird_name": "Northern Hawk Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297367931/1200",  # noqa
    },
    {
        "bird_name": "Northern Pygmy-Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297378121/1200",  # noqa
    },
    {
        "bird_name": "Burrowing Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297386341/1200",  # noqa
    },
    {
        "bird_name": "Barred Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297388681/1200",  # noqa
    },
    {
        "bird_name": "Great Gray Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/32804161/1200",  # noqa
    },
    {
        "bird_name": "Long-eared Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297391881/1200",  # noqa
    },
    {
        "bird_name": "Short-eared Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297400191/1200",  # noqa
    },
    {
        "bird_name": "Boreal Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/70095811/1200",  # noqa
    },
    {
        "bird_name": "Northern Saw-whet Owl",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297402651/1200",  # noqa
    },
    {
        "bird_name": "Common Nighthawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303958241/1200",  # noqa
    },
    {
        "bird_name": "Common Poorwill",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303970111/1200",  # noqa
    },
    {
        "bird_name": "Black Swift",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304079521/1200",  # noqa
    },
    {
        "bird_name": "Vaux's Swift",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304091041/1200",  # noqa
    },
    {
        "bird_name": "White-throated Swift",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304094121/1200",  # noqa
    },
    {
        "bird_name": "Black-chinned Hummingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303887411/1200",  # noqa
    },
    {
        "bird_name": "Anna's Hummingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303891031/1200",  # noqa
    },
    {
        "bird_name": "Calliope Hummingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/65766661/1200",  # noqa
    },
    {
        "bird_name": "Broad-tailed Hummingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303992401/1200",  # noqa
    },
    {
        "bird_name": "Rufous Hummingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/68934031/1200",  # noqa
    },
    {
        "bird_name": "Belted Kingfisher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303715931/1200",  # noqa
    },
    {
        "bird_name": "Lewis's Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297680501/1200",  # noqa
    },
    {
        "bird_name": "Red-naped Sapsucker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297659541/1200",  # noqa
    },
    {
        "bird_name": "Williamson's Sapsucker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297654021/1200",  # noqa
    },
    {
        "bird_name": "Downy Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60397941/1200",  # noqa
    },
    {
        "bird_name": "Hairy Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/309038471/1200",  # noqa
    },
    {
        "bird_name": "Three-toed Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297689221/1200",  # noqa
    },
    {
        "bird_name": "Black-backed Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297690371/1200",  # noqa
    },
    {
        "bird_name": "Northern Flicker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60403261/1200",  # noqa
    },
    {
        "bird_name": "Pileated Woodpecker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311372981/1200",  # noqa
    },
    {
        "bird_name": "Olive-sided Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301807021/1200",  # noqa
    },
    {
        "bird_name": "Western Wood-Pewee",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301812111/1200",  # noqa
    },
    {
        "bird_name": "Willow Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301824711/1200",  # noqa
    },
    {
        "bird_name": "American Redstart",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297029071/1200",  # noqa
    },
    {
        "bird_name": "Northern Waterthrush",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296756901/1200",  # noqa
    },
    {
        "bird_name": "MacGillivray's Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296834351/1200",  # noqa
    },
    {
        "bird_name": "Common Yellowthroat",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297027971/1200",  # noqa
    },
    {
        "bird_name": "Wilson's Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297073391/1200",  # noqa
    },
    {
        "bird_name": "Yellow-breasted Chat",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64916141/1200",  # noqa
    },
    {
        "bird_name": "Western Tanager",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297084201/1200",  # noqa
    },
    {
        "bird_name": "Black-headed Grosbeak",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297327641/1200",  # noqa
    },
    {
        "bird_name": "Lazuli Bunting",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311473551/1200",  # noqa
    },
    {
        "bird_name": "Spotted Towhee",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299678301/1200",  # noqa
    },
    {
        "bird_name": "American Tree Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297719081/1200",  # noqa
    },
    {
        "bird_name": "Chipping Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64972021/1200",  # noqa
    },
    {
        "bird_name": "Clay-colored Sparrow ",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64972451/1200",  # noqa
    },
    {
        "bird_name": "Brewer's Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64973431/1200",  # noqa
    },
    {
        "bird_name": "Vesper Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299337031/1200",  # noqa
    },
    {
        "bird_name": "Lark Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64973941/1200",  # noqa
    },
    {
        "bird_name": "Savannah Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299412471/1200",  # noqa
    },
    {
        "bird_name": "Grasshopper Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308767421/1200",  # noqa
    },
    {
        "bird_name": "Fox Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/66115301/1200",  # noqa
    },
    {
        "bird_name": "Song Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308771371/1200",  # noqa
    },
    {
        "bird_name": "Lincoln's Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/299637291/1200",  # noqa
    },
    {
        "bird_name": "White-throated Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64980371/1200",  # noqa
    },
    {
        "bird_name": "White-crowned Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64978031/1200",  # noqa
    },
    {
        "bird_name": "Harris's Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64979771/1200",  # noqa
    },
    {
        "bird_name": "Dark-eyed Junco",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/66115711/1200",  # noqa
    },
    {
        "bird_name": "Lapland Longspur",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/171432921/1200",  # noqa
    },
    {
        "bird_name": "Snow Bunting",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67450281/1200",  # noqa
    },
    {
        "bird_name": "Bobolink",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306331051/1200",  # noqa
    },
    {
        "bird_name": "Red-winged Blackbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306392131/1200",  # noqa
    },
    {
        "bird_name": "Western Meadowlark",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306332801/1200",  # noqa
    },
    {
        "bird_name": "Yellow-headed Blackbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306328961/1200",  # noqa
    },
    {
        "bird_name": "Brewer's Blackbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306625401/1200",  # noqa
    },
    {
        "bird_name": "Common Grackle",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/307948931/1200",  # noqa
    },
    {
        "bird_name": "Brown-headed Cowbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306621721/1200",  # noqa
    },
    {
        "bird_name": "Bullock's Oriole",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306361551/1200",  # noqa
    },
    {
        "bird_name": "Gray-crowned Rosy-Finch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/307951151/1200",  # noqa
    },
    {
        "bird_name": "Black Rosy-Finch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308040821/1200",  # noqa
    },
    {
        "bird_name": "Pine Grosbeak",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67356151/1200",  # noqa
    },
    {
        "bird_name": "Purple Finch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306333791/1200",  # noqa
    },
    {
        "bird_name": "Cassin's Finch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67283591/1200",  # noqa
    },
    {
        "bird_name": "House Finch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306327341/1200",  # noqa
    },
    {
        "bird_name": "Red Crossbill",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306363961/1200",  # noqa
    },
    {
        "bird_name": "White-winged Crossbill",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/171455081/1200",  # noqa
    },
    {
        "bird_name": "Common Redpoll",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311370301/1200",  # noqa
    },
    {
        "bird_name": "Pine Siskin",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306420291/1200",  # noqa
    },
    {
        "bird_name": "American Goldfinch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306710541/1200",  # noqa
    },
    {
        "bird_name": "Evening Grosbeak",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311371841/1200",  # noqa
    },
    {
        "bird_name": "House Sparrow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305880301/1200",  # noqa
    },
    {
        "bird_name": "Harlequin Duck",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63898051/1200",  # noqa
    },
    {
        "bird_name": "Long-tailed Duck",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302051251/1200",  # noqa
    },
    {
        "bird_name": "Surf Scoter",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302040411/1200",  # noqa
    },
    {
        "bird_name": "White-winged Scoter",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302047121/1200",  # noqa
    },
    {
        "bird_name": "Common Goldeneye",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302057041/1200",  # noqa
    },
    {
        "bird_name": "Barrow's Goldeneye",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302060441/1200",  # noqa
    },
    {
        "bird_name": "Bufflehead",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/216531741/1200",  # noqa
    },
    {
        "bird_name": "Hooded Merganser",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302071781/1200",  # noqa
    },
    {
        "bird_name": "Common Merganser",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63910971/1200",  # noqa
    },
    {
        "bird_name": "Red-breasted Merganser",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302119101/1200",  # noqa
    },
    {
        "bird_name": "Ruddy Duck",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302124271/1200",  # noqa
    },
    {
        "bird_name": "Osprey",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60320581/1200",  # noqa
    },
    {
        "bird_name": "Bald Eagle",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306062281/1200",  # noqa
    },
    {
        "bird_name": "Northern Harrier",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305647471/1200",  # noqa
    },
    {
        "bird_name": "Sharp-shinned Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305676551/1200",  # noqa
    },
    {
        "bird_name": "Cooper's Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60324921/1200",  # noqa
    },
    {
        "bird_name": "Northern Goshawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306057361/1200",  # noqa
    },
    {
        "bird_name": "Swainson's Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/71545031/1200",  # noqa
    },
    {
        "bird_name": "Red-tailed Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/60384771/1200",  # noqa
    },
    {
        "bird_name": "Ferruginous Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306323611/1200",  # noqa
    },
    {
        "bird_name": "Rough-legged Hawk",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/71545911/1200",  # noqa
    },
    {
        "bird_name": "Golden Eagle",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305364081/1200",  # noqa
    },
    {
        "bird_name": "American Kestrel",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302366931/1200",  # noqa
    },
    {
        "bird_name": "Merlin",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303424671/1200",  # noqa
    },
    {
        "bird_name": "Peregrine Falcon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303618951/1200",  # noqa
    },
    {
        "bird_name": "Gyrfalcon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/170735401/1200",  # noqa
    },
    {
        "bird_name": "Prairie Falcon",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/71547471/1200",  # noqa
    },
    {
        "bird_name": "Gray Partridge",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296750991/1200",  # noqa
    },
    {
        "bird_name": "Chukar",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296741481/1200",  # noqa
    },
    {
        "bird_name": "Ring-necked Pheasant",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296747851/1200",  # noqa
    },
    {
        "bird_name": "Spruce Grouse",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295442571/1200",  # noqa
    },
    {
        "bird_name": "Blue Grouse",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/39523291/1200",  # noqa
    },
    {
        "bird_name": "White-tailed Ptarmigan",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295452811/1200",  # noqa
    },
    {
        "bird_name": "Ruffed Grouse",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/65615501/900",  # noqa
    },
    {
        "bird_name": "Sharp-tailed Grouse",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295483111/900",  # noqa
    },
    {
        "bird_name": "Wild Turkey",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311368611/2400",  # noqa
    },
    {
        "bird_name": "Virginia Rail",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/37883281/900",  # noqa
    },
    {
        "bird_name": "Sora",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/164608031/900",  # noqa
    },
    {
        "bird_name": "American Coot",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303193191/900",  # noqa
    },
    {
        "bird_name": "Sandhill Crane",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297925101/900",  # noqa
    },
    {
        "bird_name": "Black-bellied Plover",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297925101/900",  # noqa
    },
    {
        "bird_name": "American Golden-Plover",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297937901/900",  # noqa
    },
    {
        "bird_name": "Semipalmated Plover",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301215411/900",  # noqa
    },
    {
        "bird_name": "Killdeer",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301221881/900",  # noqa
    },
    {
        "bird_name": "Black-necked Stilt",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297914651/900",  # noqa
    },
    {
        "bird_name": "American Avocet",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297916791/900",  # noqa
    },
    {
        "bird_name": "Greater Yellowlegs",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64836521/900",  # noqa
    },
    {
        "bird_name": "Lesser Yellowlegs",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301797021/900",  # noqa
    },
    {
        "bird_name": "Solitary Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301786351/900",  # noqa
    },
    {
        "bird_name": "Willet",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64838171/900",  # noqa
    },
    {
        "bird_name": "Spotted Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301783811/900",  # noqa
    },
    {
        "bird_name": "Upland Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301229341/900",  # noqa
    },
    {
        "bird_name": "Long-billed Curlew",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64810681/900",  # noqa
    },
    {
        "bird_name": "Marbled Godwit",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64811431/900",  # noqa
    },
    {
        "bird_name": "Sanderling",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/64814541/900",  # noqa
    },
    {
        "bird_name": "Semipalmated Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301537841/900",  # noqa
    },
    {
        "bird_name": "Western Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301565971/900",  # noqa
    },
    {
        "bird_name": "Least Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301499501/900",  # noqa
    },
    {
        "bird_name": "Baird's Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301498711/900",  # noqa
    },
    {
        "bird_name": "Pectoral Sandpiper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301535181/900",  # noqa
    },
    {
        "bird_name": "Dunlin",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301478601/900",  # noqa
    },
    {
        "bird_name": "Least Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301828001/900",  # noqa
    },
    {
        "bird_name": "Hammond's Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301849191/900",  # noqa
    },
    {
        "bird_name": "Dusky Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301858411/900",  # noqa
    },
    {
        "bird_name": "Cordilleran Flycatcher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301864901/900",  # noqa
    },
    {
        "bird_name": "Say's Phoebe",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/301881791/900",  # noqa
    },
    {
        "bird_name": "Western Kingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/65683361/900",  # noqa
    },
    {
        "bird_name": "Eastern Kingbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302123291/900",  # noqa
    },
    {
        "bird_name": "Horned Lark",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308603561/900",  # noqa
    },
    {
        "bird_name": "Tree Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305568151/900",  # noqa
    },
    {
        "bird_name": "Violet-green Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305584951/900",  # noqa
    },
    {
        "bird_name": "N. Rough-winged Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311350051/900",  # noqa
    },
    {
        "bird_name": "Bank Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305597801/900",  # noqa
    },
    {
        "bird_name": "Cliff Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305772321/900",  # noqa
    },
    {
        "bird_name": "Barn Swallow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/312652671/900",  # noqa
    },
    {
        "bird_name": "Gray Jay",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/70582941/1200",  # noqa
    },
    {
        "bird_name": "Steller's Jay",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/70582731/900",  # noqa
    },
    {
        "bird_name": "Blue Jay",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/311635911/900",  # noqa
    },
    {
        "bird_name": "Clark's Nutcracker",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/70580641/900",  # noqa
    },
    {
        "bird_name": "Black-billed Magpie",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302389441/900",  # noqa
    },
    {
        "bird_name": "American Crow",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/59858041/900",  # noqa
    },
    {
        "bird_name": "Common Raven",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/300152741/900",  # noqa
    },
    {
        "bird_name": "Black-capped Chickadee",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302472691/900",  # noqa
    },
    {
        "bird_name": "Mountain Chickadee",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302474581/900",  # noqa
    },
    {
        "bird_name": "Chestnut-backed Chickadee",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302487051/900",  # noqa
    },
    {
        "bird_name": "Red-breasted Nuthatch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308563981/900",  # noqa
    },
    {
        "bird_name": "White-breasted Nuthatch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/68039391/900",  # noqa
    },
    {
        "bird_name": "Pygmy Nuthatch",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308592271/900",  # noqa
    },
    {
        "bird_name": "Brown Creeper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308597451/900",  # noqa
    },
    {
        "bird_name": "Rock Wren",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308758461/900",  # noqa
    },
    {
        "bird_name": "Canyon Wren",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304320881/900",  # noqa
    },
    {
        "bird_name": "House Wren",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/59860711/900",  # noqa
    },
    {
        "bird_name": "Winter Wren",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/304456681/900",  # noqa
    },
    {
        "bird_name": "Marsh Wren",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/68035321/900",  # noqa
    },
    {
        "bird_name": "American Dipper",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303932641/900",  # noqa
    },
    {
        "bird_name": "Golden-crowned Kinglet",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/161342541/900",  # noqa
    },
    {
        "bird_name": "Ruby-crowned Kinglet",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67474361/900",  # noqa
    },
    {
        "bird_name": "Western Bluebird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67472541/900",  # noqa
    },
    {
        "bird_name": "Mountain Bluebird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303800251/900",  # noqa
    },
    {
        "bird_name": "Townsend's Solitaire",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67469871/900",  # noqa
    },
    {
        "bird_name": "Veery",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303691281/900",  # noqa
    },
    {
        "bird_name": "Swainson's Thrush",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/67460821/900",  # noqa
    },
    {
        "bird_name": "Hermit Thrush",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303450651/900",  # noqa
    },
    {
        "bird_name": "American Robin",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303441381/900",  # noqa
    },
    {
        "bird_name": "Varied Thrush",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303793391/900",  # noqa
    },
    {
        "bird_name": "Gray Catbird",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303925501/900",  # noqa
    },
    {
        "bird_name": "Sage Thrasher",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303814901/900",  # noqa
    },
    {
        "bird_name": "American Pipit",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306149371/900",  # noqa
    },
    {
        "bird_name": "Bohemian Waxwing",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/162296061/900",  # noqa
    },
    {
        "bird_name": "Cedar Waxwing",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/305837211/900",  # noqa
    },
    {
        "bird_name": "Northern Shrike",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302342581/900",  # noqa
    },
    {
        "bird_name": "Loggerhead Shrike",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302339191/900",  # noqa
    },
    {
        "bird_name": "European Starling",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/303928891/900",  # noqa
    },
    {
        "bird_name": "Cassin's Vireo",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/525120381/900",  # noqa
    },
    {
        "bird_name": "Warbling Vireo",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302322781/900",  # noqa
    },
    {
        "bird_name": "Red-eyed Vireo",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/302325181/900",  # noqa
    },
    {
        "bird_name": "Tennessee Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296780741/900",  # noqa
    },
    {
        "bird_name": "Orange-crowned Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296785521/900",  # noqa
    },
    {
        "bird_name": "Nashville Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/296807801/900",  # noqa
    },
    {
        "bird_name": "Yellow Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297046671/900",  # noqa
    },
    {
        "bird_name": "Yellow-rumped Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/308764651/900",  # noqa
    },
    {
        "bird_name": "Townsend's Warbler",
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/297064161/900",  # noqa
    },
]


def create_bird(bird):
    with pool.connection() as conn:
        with conn.cursor() as db:
            db.execute(
                """
                INSERT INTO birds
                    (bird_name
                    , picture_url)
                VALUES
                    (%s, %s)
                RETURNING id;
                """,
                [
                    bird["bird_name"],
                    bird["picture_url"],
                ],
            )
    return True


def get_all_birds():
    with pool.connection() as conn:
        with conn.cursor() as db:
            db.execute(
                """
                SELECT bird_name
                FROM birds
                """
            )
            results = []
            for row in db.fetchall():
                birds = {}
                for i, col in enumerate(db.description):
                    birds = row[i]
                results.append(birds)
            return results


all_birds = get_all_birds()

for bird in birds:
    if bird["bird_name"] not in all_birds:
        response = create_bird(bird)
